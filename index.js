window.onload = function () {
    function fn(name) {
        return document.querySelector(name)
    }
    // 轮播图
    let imgList = [
        "./images/lb1.jpg",
        "./images/lb2.jpg",
        "./images/lb3.jpg",
        "./images/lb4.jpg",
        "./images/lb5.jpg",
        "./images/lb6.jpg",
        "./images/lb7.jpg",
        "./images/lb8.jpg",
        "./images/lb9.jpg",
        "./images/lb10.jpg",
        "./images/lb11.jpg",
        "./images/lb12.jpg",
    ]
    const img1 = fn(".slider-img1")
    const img2 = fn(".slider-img2")
    const switchs = document.querySelectorAll('.main-right-switch a')
    let i = 0
    let j = 0
    img1.src = imgList[i]
    img2.src = imgList[i + 1]
    switchs[i].classList.add('selected')
    let slidertimer = setInterval(function () {
        // i = i + 1
        // if (i >= switchs.length) {
        //     i = 0
        //     switchs[i].classList.add('selected')
        //     switchs[switchs.length - 1].classList.remove('selected')

        // }
        // img1.src = imgList[i + 1]
        // img2.src = imgList[i + 2]

        // switchs[i - 1].classList.remove('selected')
        // switchs[i].classList.add('selected')
        i = i + 2
        if (i >= switchs.length) {
            i = 0
        }
        img1.src = imgList[i]
        img2.src = imgList[i + 2]
        if (j >= switchs.length - 1) {
            switchs[j].classList.remove('selected')
            j = 0
            switchs[j].classList.add('selected')

        } else {
            switchs[j].classList.remove('selected')
            switchs[j + 1].classList.add('selected')
            j++
        }



    }, 1000)
    // 播放器
    let musicList = [
        "./music/ICU (避风港).mp3",
        "./music/Attention.mp3",
        "./music/Hype Boy.mp3",
        "./music/Life's Too Short .mp3",
        "./music/More&More.mp3",
        "./music/One Last Kiss.mp3"

    ];
    let titleList = [
        "aespa - ICU (避风港)",
        "NewJeans (뉴진스) - Attention",
        "NewJeans (뉴진스) - Hype Boy",
        "aespa - Life's Too Short (English Ver.)",
        "More&More DEMO",
        "宇多田ヒカル - One Last Kiss"

    ];
    let num = 1
    fn(".audio").src = musicList[num]
    fn(".musicname").innerHTML = titleList[num]
    let audio = fn(".audio")
    fn(".prev").onclick = function () {
        num--;
        if (num <= 0) {
            num = musicList.length
        }
        fn(".audio").src = musicList[num]
        fn(".musicname").innerHTML = titleList[num]
        audio.play()
    }
    fn(".next").onclick = function () {
        num++;
        if (num >= musicList.length) {
            num = 0
        }
        fn(".audio").src = musicList[num]
        fn(".musicname").innerHTML = titleList[num]
        audio.play()

    }

}